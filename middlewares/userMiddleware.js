const getAllUser = (req,res,next)=>{
    console.log("Get All Users!");
    next();
};

const getAUser = (req,res,next)=>{
    console.log("Get A User!");
    next();
};

const postAUser = (req,res,next)=>{
    console.log("Create New User!");
    next();
};

const putAUser = (req,res,next)=>{
    console.log("Update A User!");
    next();
};

const deleteAUser = (req,res,next)=>{
    console.log("Delete A User!");
    next();
};

module.exports = {
    getAllUser,
    getAUser,
    postAUser,
    putAUser,
    deleteAUser
}