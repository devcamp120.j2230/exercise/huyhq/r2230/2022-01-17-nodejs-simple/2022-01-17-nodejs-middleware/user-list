const express = require("express");
const {
    getAllUser,
    getAUser,
    postAUser,
    putAUser,
    deleteAUser
} = require("../middlewares/UserMiddleware");

const userRoute = express.Router();


userRoute.get("/",getAllUser,(req,res)=>{
    res.status(200).json({
        message: "Get All Users!",
    })
})

userRoute.get("/:id",getAUser,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Get User id = "+id,
    })
})

userRoute.post("/:id",postAUser,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Create User id = "+id,
    })
})

userRoute.put("/:id",putAUser,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Update User id = "+id,
    })
})

userRoute.delete("/:id",deleteAUser,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Delete User id = "+id,
    })
})

module.exports = {userRoute};