//khai báo thư viện express
const express = require("express");

const {userRoute} = require("./routes/userRoute");
//khởi tạo app NodeJs
const app = express();

//khai báo cổng chạy ứng dụng
const port = 8000;

app.use("/", userRoute);

//chạy ứng dụng trên cổng
app.listen(port, () => {
    console.log("App running on port: " + port);
})